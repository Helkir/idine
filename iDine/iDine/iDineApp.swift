//
//  iDineApp.swift
//  iDine
//
//  Created by Arnaud Chrétien on 02/09/2021.
//

import SwiftUI

@main
struct iDineApp: App {
    @StateObject var order = Order()
    
    var body: some Scene {
        WindowGroup {
            MainView()
                .environmentObject(order)
        }
    }
}
