//
//  ContentView.swift
//  iDine
//
//  Created by Arnaud Chrétien on 02/09/2021.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var order: Order
    let menu = Bundle.main.decode([MenuSection].self, from: "menu.json")
    
    var body: some View {
        NavigationView {
            List {
            ForEach(menu) { section in
                Section(header: Text(section.name)) {
                    ForEach(section.items) { item in
                        NavigationLink(destination: ItemDetail(item: item)) {
                            ItemRow(item: item)
                        }
                    }
                }
            }
        }
            .navigationTitle("Menu")
           // .navigationBarTitleDisplayMode(.inline)
            .listStyle(GroupedListStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
